#include <stdio.h>

int first(int);
int attend(int);
int cost(int);
int rev(int);
int profit(int);


int t;

int first(int t){
return 15-t;
}

int attend(int c){
return 120-4*first(c);
}

int cost(int d){
return attend(d)*3+500;
}

int rev( int t){
return attend(t)*t;
}

int profit(int t){
return rev(t)-cost(t);
}

int main(){

printf("Price of the ticket= ");
scanf("%d",&t);
printf("Profit=%d",profit(t));
printf("\n");

return 0;

}
